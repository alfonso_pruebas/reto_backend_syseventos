<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20230724212610 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE messenger_messages (id BIGINT AUTO_INCREMENT NOT NULL, body LONGTEXT NOT NULL, headers LONGTEXT NOT NULL, queue_name VARCHAR(190) NOT NULL, created_at DATETIME NOT NULL, available_at DATETIME NOT NULL, delivered_at DATETIME DEFAULT NULL, INDEX IDX_75EA56E0FB7336F0 (queue_name), INDEX IDX_75EA56E0E3BD61CE (available_at), INDEX IDX_75EA56E016BA31DB (delivered_at), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('DROP INDEX FK_uscrcrea ON evento');
        $this->addSql('ALTER TABLE evento ADD precio DOUBLE PRECISION DEFAULT NULL, DROP FechaCreacion, CHANGE Nombre nombre VARCHAR(200) NOT NULL, CHANGE Descripcion descripcion VARCHAR(350) NOT NULL, CHANGE Fecha fecha VARCHAR(200) NOT NULL, CHANGE Id_Tipo_Evento id_tipo_evento VARCHAR(255) NOT NULL, CHANGE Id_Categoria id_categoria VARCHAR(255) NOT NULL, CHANGE Id_Usuario_Creacion id_usuario_creacion INT NOT NULL');
        $this->addSql('ALTER TABLE usuario DROP FechaCreacion, CHANGE Username username VARCHAR(200) NOT NULL, CHANGE Password password VARCHAR(200) NOT NULL, CHANGE Id_Tipo_Usuario id_tipo_usuario INT NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE messenger_messages');
        $this->addSql('ALTER TABLE evento ADD FechaCreacion DATETIME DEFAULT CURRENT_TIMESTAMP, DROP precio, CHANGE nombre Nombre VARCHAR(200) DEFAULT NULL, CHANGE descripcion Descripcion VARCHAR(350) DEFAULT NULL, CHANGE fecha Fecha DATE DEFAULT NULL, CHANGE id_tipo_evento Id_Tipo_Evento VARCHAR(200) DEFAULT NULL, CHANGE id_categoria Id_Categoria VARCHAR(200) DEFAULT NULL, CHANGE id_usuario_creacion Id_Usuario_Creacion INT DEFAULT NULL');
        $this->addSql('CREATE INDEX FK_uscrcrea ON evento (Id_Usuario_Creacion)');
        $this->addSql('ALTER TABLE usuario ADD FechaCreacion DATETIME DEFAULT CURRENT_TIMESTAMP, CHANGE username Username VARCHAR(200) DEFAULT NULL, CHANGE password Password VARCHAR(200) DEFAULT NULL, CHANGE id_tipo_usuario Id_Tipo_Usuario INT DEFAULT NULL');
    }
}
