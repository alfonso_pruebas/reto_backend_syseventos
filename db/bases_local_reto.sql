-- --------------------------------------------------------

-- Host:                         127.0.0.1

-- Versión del servidor:         5.7.29 - MySQL Community Server (GPL)

-- SO del servidor:              Linux

-- HeidiSQL Versión:             12.4.0.6659

-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */

;

/*!40101 SET NAMES utf8 */

;

/*!50503 SET NAMES utf8mb4 */

;

/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */

;

/*!40103 SET TIME_ZONE='+00:00' */

;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */

;

/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */

;

/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */

;

-- Volcando estructura de base de datos para symfony

CREATE DATABASE
    IF NOT EXISTS `symfony`
    /*!40100 DEFAULT CHARACTER SET utf8 */
;

USE `symfony`;

-- Volcando estructura para tabla symfony.doctrine_migration_versions

CREATE TABLE
    IF NOT EXISTS `doctrine_migration_versions` (
        `version` varchar(191) COLLATE utf8_unicode_ci NOT NULL,
        `executed_at` datetime DEFAULT NULL,
        `execution_time` int(11) DEFAULT NULL,
        PRIMARY KEY (`version`)
    ) ENGINE = InnoDB DEFAULT CHARSET = utf8 COLLATE = utf8_unicode_ci;

-- Volcando datos para la tabla symfony.doctrine_migration_versions: ~0 rows (aproximadamente)

-- Volcando estructura para tabla symfony.evento

CREATE TABLE
    IF NOT EXISTS `evento` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `Nombre` varchar(200) DEFAULT NULL,
        `Descripcion` varchar(350) DEFAULT NULL,
        `Fecha` date DEFAULT NULL,
        `precio` double DEFAULT NULL,
        `Id_Tipo_Evento` varchar(200) DEFAULT NULL,
        `Id_Categoria` varchar(200) DEFAULT NULL,
        `Id_Usuario_Creacion` int(11) DEFAULT NULL,
        `FechaCreacion` datetime DEFAULT CURRENT_TIMESTAMP,
        PRIMARY KEY (`id`) USING BTREE,
        KEY `FK_uscrcrea` (`Id_Usuario_Creacion`) USING BTREE
    ) ENGINE = InnoDB AUTO_INCREMENT = 28 DEFAULT CHARSET = utf8 COMMENT = 'catalogo de eventos';