<?php

namespace App\Entity;

use App\Repository\EventoRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: EventoRepository::class)]
class Evento
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 200)]
    private ?string $Nombre = null;

    #[ORM\Column(length: 350)]
    private ?string $Descripcion = null;

    #[ORM\Column(length: 200)]
    private ?string $Fecha = null;

    #[ORM\Column]
    private ?string $IdTipoEvento = null;

    #[ORM\Column]
    private ?string $IdCategoria = null;

    #[ORM\Column]
    private ?int $IdUsuarioCreacion = null;

    #[ORM\Column(nullable: true)]
    private ?float $Precio = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombre(): ?string
    {
        return $this->Nombre;
    }

    public function setNombre(string $Nombre): static
    {
        $this->Nombre = $Nombre;

        return $this;
    }

    public function getDescripcion(): ?string
    {
        return $this->Descripcion;
    }

    public function setDescripcion(string $Descripcion): static
    {
        $this->Descripcion = $Descripcion;

        return $this;
    }

    public function getFecha(): ?string
    {
        return $this->Fecha;
    }

    public function setFecha(string $Fecha): static
    {
        $this->Fecha = $Fecha;

        return $this;
    }

    public function getIdTipoEvento(): ?string
    {
        return $this->IdTipoEvento;
    }

    public function setIdTipoEvento(string $IdTipoEvento): static
    {
        $this->IdTipoEvento = $IdTipoEvento;

        return $this;
    }

    public function getIdCategoria(): ?string
    {
        return $this->IdCategoria;
    }

    public function setIdCategoria(string $IdCategoria): static
    {
        $this->IdCategoria = $IdCategoria;

        return $this;
    }

    public function getIdUsuarioCreacion(): ?int
    {
        return $this->IdUsuarioCreacion;
    }

    public function setIdUsuarioCreacion(int $IdUsuarioCreacion): static
    {
        $this->IdUsuarioCreacion = $IdUsuarioCreacion;

        return $this;
    }

    public function getPrecio(): ?float
    {
        return $this->Precio;
    }

    public function setPrecio(?float $Precio): static
    {
        $this->Precio = $Precio;

        return $this;
    }

}
