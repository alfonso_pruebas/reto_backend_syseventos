<?php
namespace App\Controller;


use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

class Usuarios_controller  extends AbstractController{

    
     public function __construct() {

     }

    /*     
      User/tabla  name="User_tabla"
     */
    public function tabla(Request $request, UserRepository $userrepository, PaginatorInterface $paginator): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Solo los usuarios pueden acceder ');

        $query = $userrepository->obtentodos();
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('User/user_lista.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /*     
      User/borrar  name="User_borrar"
     */
    public function borrar_usuario(Request $request, EntityManagerInterface $emi, UserRepository $userrepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Solo los administradores pueden acceder ');

        $id = $request->get('id', null);
        $user = $userrepository->find($id);
        
        $emi->remove($user);
        $emi->flush();


        return $this->redirectToRoute('User_tabla');
    }
}
