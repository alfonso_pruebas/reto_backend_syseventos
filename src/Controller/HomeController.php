<?php

namespace App\Controller;


use App\Repository\EventoRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/home', name: 'app_home')]
    public function index( EventoRepository $eventoRepository): Response
    {

        $eventos_of = $eventoRepository->findBy(array('IdCategoria' => 'En oferta'), array('Nombre' => 'ASC'),5,0);

        $eventos_pr = $eventoRepository->findBy(array('IdCategoria' => 'Proximos'), array('Nombre' => 'ASC'), 5,0);

        $eventos_pa = $eventoRepository->findBy(array('IdCategoria' => 'Pasados'), array('Nombre' => 'ASC'), 5,0);



        return $this->render('home/index.html.twig', [
            'eventos_oferta' => $eventos_of,
            'eventos_proximos' => $eventos_pr,
            'eventos_pasados' => $eventos_pa,
        ]);
    }
}
