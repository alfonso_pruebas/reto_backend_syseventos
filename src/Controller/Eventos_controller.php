<?php
namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Evento;
use App\Form\EventoformType;
use App\Repository\EventoRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

class Eventos_controller  extends AbstractController{

    
     public function __construct() {

     }

    /*     
      Evento/tabla  name="Evento_tabla"
     */
    public function tabla(Request $request, EventoRepository $eventoRepository, PaginatorInterface $paginator): Response
    {
        $this->denyAccessUnlessGranted('ROLE_USER', null, 'Solo los usuarios pueden acceder ');
        

        $query = $eventoRepository->obtentodos();
        $pagination = $paginator->paginate(
            $query, /* query NOT result */
            $request->query->getInt('page', 1), /*page number*/
            10 /*limit per page*/
        );

        return $this->render('Eventos/evento_lista.html.twig', [
            'pagination' => $pagination,
        ]);
    }

    /*     
      Evento/alta  name="Evento_alta"
     */
    public function alta(Request $request, EntityManagerInterface $emi): Response
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Solo los administradores pueden acceder ');
        $evento = new Evento();
        // ...

        $form = $this->createForm(EventoformType::class, $evento);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
            $emi->persist($evento);
            $emi->flush();
            return $this->redirectToRoute('Evento_tabla');
        }

        return $this->render('Eventos/evento_alta.html.twig', [
            'form' => $form->createView(),
            'tipo' => 'alta'
        ]);
    }

    /*     
      Evento/lista  name="Evento_lista"
     */
    public function lista(Request $request, EventoRepository $eventoRepository){

        $eventos_arr = array();
        $eventos = $eventoRepository->findAll();
        foreach ($eventos as $evento) {
            $eventos_arr[] = [
                'id'=>$evento->getId(),
                'nombre'=>$evento->getNombre(),
                'descripcion'=>$evento->getDescripcion(),
                'Fecha'=>$evento->getFecha(),
                'Precio' => $evento->getPrecio(),
                'idtipoevento'=>$evento->getIdTipoEvento(),
                'idcategoria'=>$evento->getIdCategoria(),
                'idusuariocreacion'=>$evento->getIdUsuarioCreacion()
            ];
        }
        
        $data = [ 'success' => true,
                    'data' => $eventos_arr];

        $response = new JsonResponse();
        $response->setData($data);
        return $response;
    }

    /**
     * Evento/listapor  name="Evento_listapor"
     */
    public function lista_especifica(Request $request, EventoRepository $eventoRepository)
    {
        $eventos_arr = array();
        $response = new JsonResponse();

        $campo = $request->get('campo', null);
        $valor = $request->get('valor', null);
        if(empty($campo) || empty($valor)){
            $response->setData(
                ['success' => false,
                    'error' => 'Sin criterios para filtrar ',
                    'data' => null]
            );
            return $response;
        }
        $eventos = $eventoRepository->findBy(array($campo=> $valor),array('Nombre'=> 'ASC'));
        
        foreach ($eventos as $evento) {
            $eventos_arr[] = [
                'id' => $evento->getId(),
                'nombre' => $evento->getNombre(),
                'descripcion' => $evento->getDescripcion(),
                'Fecha' => $evento->getFecha(),
                'idtipoevento' => $evento->getIdTipoEvento(),
                'idcategoria' => $evento->getIdCategoria(),
                'idusuariocreacion' => $evento->getIdUsuarioCreacion()
            ];
        }

        $data = [
            'success' => true,
            'data' => $eventos_arr
        ];
       
        $response->setData($data);
        return $response;
    }

    /**
     * @Route("/Evento/nuevo", name="Evento_nuevo")
     */
    public function nuevo_evento(Request $request,EntityManagerInterface $emi){
        $evento = new Evento();
        $response = new JsonResponse();

        $nombre = $request->get('nombre',null);
        $descripcion = $request->get('descripcion', null);
        $fecha = $request->get('fecha', null);
        $idtipoevento = $request->get('idtipoevento', null);
        $idcategoria = $request->get('idcategoria', null);
        $idusuariocreacion = $request->get('idusuariocreacion', null);

        if(empty($nombre) || empty($descripcion) || empty($fecha) || empty($idtipoevento) || empty($idcategoria) || empty($idusuariocreacion)){

            $response->setData(
                [
                    'success' => false,
                    'error' => 'Todos los campos deben tener un valor ',
                    'data' => null
                ]);
            return $response;
        }

        $evento->setNombre($nombre);
        $evento->setDescripcion($descripcion);
        $evento->setFecha($fecha);
        $evento->setIdTipoEvento($idtipoevento);
        $evento->setIdCategoria($idcategoria);
        $evento->setIdUsuarioCreacion($idusuariocreacion);

        $emi->persist($evento);
        $emi->flush();

        
        $response->setData(
            [
                'success'=> true,
                'data'=>[
                    [
                        'id'=>$evento->getId(),
                        'nombre'=>$evento->getNombre(),
                        'descripcion'=>$evento->getDescripcion(),
                        'Fecha'=>$evento->getFecha(),
                        'idtipoevento'=>$evento->getIdTipoEvento(),
                        'idcategoria'=>$evento->getIdCategoria(),
                        'idusuariocreacion'=>$evento->getIdUsuarioCreacion()
                    ]
                ]
            ]);

        return $response;    
    }

    //  /Evento/borrar   name="Evento_borrar"
    public function borrar_evento(Request $request, EntityManagerInterface $emi, EventoRepository $eventoRepository){
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Solo los administradores pueden acceder ');

        $id = $request->get('id', null);
        $evento = $eventoRepository->find($id);


        $emi->remove($evento);
        $emi->flush();

        
        return $this->redirectToRoute('Evento_tabla');

    }

    // /Evento/actualizar   name="Evento_actualizar"
    public function actualiza_evento(Request $request, EntityManagerInterface $emi, EventoRepository $eventoRepository)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Solo los administradores pueden acceder ');
        $id = $request->get('id', null);
        $evento = $eventoRepository->find($id);

        $form_evento = $this->createForm(EventoformType::class,$evento);
        $form_evento->handleRequest($request);

        if($form_evento->isSubmitted() && $form_evento->isValid()){
            $emi->persist($evento);
            $emi->flush();
            return $this->redirectToRoute('Evento_tabla');
        }


        return $this->render('Eventos/evento_alta.html.twig', [
            'form' => $form_evento->createView(),
            'tipo'=> 'actualizacion'
        ]);
    }
}

?>