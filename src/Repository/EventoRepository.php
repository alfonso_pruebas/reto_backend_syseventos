<?php

namespace App\Repository;

use App\Entity\Evento;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query\ResultSetMappingBuilder;

/**
 * @extends ServiceEntityRepository<Evento>
 *
 * @method Evento|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evento|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evento[]    findAll()
 * @method Evento[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EventoRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evento::class);
    }

    public function obtentodos() {
        
       // return $this->getEntityManager()->createNativeQuery('SELECT ev.id,ev.Nombre,ev.Descripcion,ev.Fecha,ev.IdTipoEvento,ev.Precio , ev.IdCategoria,usu.email as usuarionom FROM evento ev LEFT JOIN user usu ON ev.Id_Usuario_Creacion = usu.id ', new ResultSetMapping());
        return $this->getEntityManager()->createQuery("SELECT ev.id,ev.Nombre,ev.Descripcion,ev.Fecha,ev.IdTipoEvento,ev.Precio ,ev.IdCategoria,usu.email
          FROM App:evento  ev   
          JOIN App:User  usu where  ev.IdUsuarioCreacion = usu.id ");

    }
  



//    /**
//     * @return Evento[] Returns an array of Evento objects
//     */
//    public function findByExampleField($value): array
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->orderBy('e.id', 'ASC')
//            ->setMaxResults(10)
//            ->getQuery()
//            ->getResult()
//        ;
//    }

//    public function findOneBySomeField($value): ?Evento
//    {
//        return $this->createQueryBuilder('e')
//            ->andWhere('e.exampleField = :val')
//            ->setParameter('val', $value)
//            ->getQuery()
//            ->getOneOrNullResult()
//        ;
//    }
}
