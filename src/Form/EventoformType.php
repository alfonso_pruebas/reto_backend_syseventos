<?php

namespace App\Form;

use App\Entity\Evento;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EventoformType extends AbstractType
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        
        $user = $this->security->getUser();

        $builder
            ->add('Nombre',TextType::class,[
                'label'=>'Nombre : ',
                'attr'=>[
                    'placeholder' => 'Nombre evento',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add( 'Descripcion', TextType::class, [
                'label' => 'Descripcion : ',
                'attr' => [
                    'placeholder' => 'Descripcion de evento',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add('Precio', NumberType::class, [
                'label' => 'Precio : ',
                'attr' => [
                    'placeholder' => 'Precio de evento',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
            ->add( 'Fecha', TextType::class, [
                'label' => 'Fecha : ',
                'attr' => [
                    'placeholder' => 'Año/mes/dia ',
                    'class' => 'form-control',
                    'required' => true
                ]
            ])
        
            ->add('IdTipoEvento', ChoiceType::class,[
                'label' => 'Tipo de evento : ',
                'attr' => [
                    'class' => 'form-control',
                    'required' => true
                ],
                'choices' => [
                    'Futbol' => 'Futbol',
                    'Concierto' => 'Concierto',
                    'Formula 1' => 'Formula 1',
                    'Teatro' => 'Teatro',
                ],
            ])
            ->add('IdCategoria', ChoiceType::class, [
            'label' => 'Categoria : ',
            'attr' => [
                'class' => 'form-control',
                'required' => true
            ],
            'choices' => [
                'Próximo' => 'Próximo',
                'Pasado' => 'Pasado',
                'En Oferta' => 'En Oferta'
            ],
            ])
            ->add('IdUsuarioCreacion', HiddenType::class, [
            'label' => 'Usuario : ',
            'attr' => [
                'placeholder' => 'usuario creacion',
                'value' => $user->getId(),
                ]
            ])
            ->add('submit', SubmitType::class,[
                'label'=>'Guardar ',
                'attr' => [
                    'class' => 'btn btn-success'
                ]
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Evento::class,
        ]);
    }
}
